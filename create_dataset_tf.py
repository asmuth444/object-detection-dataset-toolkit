import argparse
import os
import sys
import shutil
import glob
from xmltodict import parse
import pandas as pd
from string import Template

DEFAULT_PARTS_FOLDER = "./parts"
DEFAULT_DATASET_PATH = "./train-tf"


def get_all_files():
    if os.path.exists(DEFAULT_PARTS_FOLDER):
        com_refs = [com_ref for com_ref in os.listdir(DEFAULT_PARTS_FOLDER)]
        com_ref_files_map = {}
        for com_ref in com_refs:
            directory = f"{DEFAULT_PARTS_FOLDER}/{com_ref}/images"
            com_ref_files_map[com_ref] = [
                {
                    "image": f"{directory}/{image}",
                    "annotation": f"{directory}/{image}".replace("images", "annotations").replace("png", "xml")
                }
                for image in sorted(os.listdir(directory))
            ]
        return com_ref_files_map
    print(f"Error: {DEFAULT_PARTS_FOLDER} does not exists")
    sys.exit(1)


def create_dataset_folder(split, com_ref_files_map, total_files):
    split /= 100
    if os.path.exists(DEFAULT_DATASET_PATH):
        print("Found existing train folder deleting it")
        shutil.rmtree(DEFAULT_DATASET_PATH)
    for folder in ["data", "training", "training/train", "training/eval"]:
        os.makedirs(f"{DEFAULT_DATASET_PATH}/{folder}", exist_ok=True)
    for com_ref in com_ref_files_map.keys():
        train_count = int(len(com_ref_files_map[com_ref]) * split)
        for idx, files in enumerate(com_ref_files_map[com_ref]):
            copy_dir = f"{DEFAULT_DATASET_PATH}/training/train" if idx <= train_count else f"{DEFAULT_DATASET_PATH}/training/eval"
            print(f"[{idx+1}/{total_files}]")
            shutil.copy(files["image"], f"{copy_dir}/{files['image'].split('/')[-1]}")
            print(f"Copied {files['image']} to {copy_dir}/{files['image'].split('/')[-1]}")
            shutil.copy(files["annotation"], f"{copy_dir}/{files['annotation'].split('/')[-1]}")
            print(f"Copied {files['annotation']} to {copy_dir}/{files['annotation'].split('/')[-1]}")
    print("Created Train Folder")


def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        with open(xml_file) as xml_file:
            annotation = parse(xml_file.read())
            annotation = annotation["annotation"]
            if type(annotation["object"]) != list:
                annotation["object"] = [annotation["object"]]
            for obj in annotation["object"]:
                xml_list += [[
                    os.path.abspath(f"{path}/{annotation['filename']}"),
                    int(annotation['size']['width']),
                    int(annotation['size']['height']),
                    obj['name'],
                    int(obj['bndbox']['xmin']),
                    int(obj['bndbox']['ymin']),
                    int(obj['bndbox']['xmax']),
                    int(obj['bndbox']['ymax'])
                ]]
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df


def create_label_csv():
    for directory in ['train', 'eval']:
        xml_df = xml_to_csv(f"{DEFAULT_DATASET_PATH}/training/{directory}")
        xml_df.to_csv(f"{DEFAULT_DATASET_PATH}/data/{directory}.csv", index=None)
        print(f"Created CSV File: {directory}.csv")


def create_pb_txt_file(com_refs):
    with open(f"{DEFAULT_DATASET_PATH}/data/labels.pbtxt", "w+") as pbtxt_file:
        item = Template("item {\n  id: $idx\n  name: '$name'\n}")
        content = "\n".join([item.substitute(idx=idx, name=com_ref) for idx, com_ref in enumerate(com_refs, start=1)])
        pbtxt_file.write(content)
        print(f"Created {DEFAULT_DATASET_PATH}/data/labels.pbtxt")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--split", type=int, default=90, help="Percentage split for training")
    opt = parser.parse_args()
    com_ref_files_map = get_all_files()
    total_files = sum(len(com_ref_files_map[com_ref]) for com_ref in com_ref_files_map.keys())
    print(f"Total Files Found: {total_files}")
    create_dataset_folder(opt.split, com_ref_files_map, total_files)
    create_label_csv()
    create_pb_txt_file(com_ref_files_map.keys())
