from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches

if __name__ == '__main__':
    idx = "092"
    im = Image.open(f"data/train-2020-01-02-0/images/train_{idx}.png")
    label_file = open(f"data/train-2020-01-02-0/labels/train_{idx}.txt")
    # im = Image.open("data/custom/images/train.jpg")
    # label_file = open("data/custom/labels/train.txt")
    im_w, im_h = im.size

    label_data = list(map(float, label_file.read().strip("\n").split(" ")))
    label_idx, x_center, y_center, width, height = label_data
    x_center *= im_w
    y_center *= im_h
    width *= im_w
    height *= im_h
    print(label_idx, x_center, y_center, width, height)
    fig, ax = plt.subplots(1)
    x_center -= width/2
    y_center -= height/2

    # Display the image
    ax.imshow(im)

    # Create a Rectangle patch
    rect = patches.Rectangle((x_center, y_center), width, height, linewidth=1, edgecolor='r', facecolor='none')

    # Add the patch to the Axes
    ax.add_patch(rect)
    plt.show()

