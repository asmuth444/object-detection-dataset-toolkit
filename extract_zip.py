import os
import sys
import zipfile
import argparse

DEFAULT_PARTS_FOLDER = './parts/'


def create_directory(path):
    if not os.path.exists(path):
        print("Creating Folder: " + path)
        os.makedirs(path, exist_ok=True)


def extract_files_from_archive_to_directory(raw_archive_path):
    with zipfile.ZipFile(raw_archive_path, "r") as zip_ref:
        for file_info in zip_ref.infolist():
            print("Extracting File " + file_info.filename)
            commercial_reference = file_info.filename.split("_")[0]
            generate_folders(commercial_reference)
            if '.xml' in file_info.filename:
                zip_ref.extract(file_info.filename, DEFAULT_PARTS_FOLDER + commercial_reference + "/annotations/")
            elif '_binary_mask.png' in file_info.filename:
                zip_ref.extract(file_info.filename, DEFAULT_PARTS_FOLDER + commercial_reference + "/masks/")
            elif '_control.png' in file_info.filename:
                zip_ref.extract(file_info.filename, DEFAULT_PARTS_FOLDER + commercial_reference + "/controls/")
            else:
                zip_ref.extract(file_info.filename, DEFAULT_PARTS_FOLDER + commercial_reference + "/images/")


def generate_folders(commercial_reference):
    directories = [
        DEFAULT_PARTS_FOLDER,
        DEFAULT_PARTS_FOLDER + commercial_reference,
        DEFAULT_PARTS_FOLDER + commercial_reference + "/annotations",
        DEFAULT_PARTS_FOLDER + commercial_reference + "/masks",
        DEFAULT_PARTS_FOLDER + commercial_reference + "/controls",
        DEFAULT_PARTS_FOLDER + commercial_reference + "/images",
    ]
    for directory in directories:
        create_directory(directory)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", type=str, default="./raw/DL.zip")
    opt = parser.parse_args()
    if not os.path.exists(opt.path):
        print("Raw archive doesn't exist: " + opt.path)
        sys.exit(1)
    print("Moving files...")
    extract_files_from_archive_to_directory(opt.path)
    print(f"{os.path} extracted")


if __name__ == "__main__":
    main()
