import argparse
import os
import shutil
from datetime import datetime

from xmltodict import parse as xmlparse

DEFAULT_DATASET_PATH = "./"
DEFAULT_PARTS_FOLDER = "./parts"
DEFAULT_CONFIG_TEMPLATE = "./config/custom_config.template"


def create_directory(path):
    if not os.path.exists(path):
        print("Creating Folder: " + path)
        os.makedirs(path, exist_ok=True)


def get_commercial_references_from_names_file(path):
    with open(path) as names_file:
        return list(
            map(
                lambda x: {"commercial-reference": x[1].strip('\n'), "label": x[0]},
                enumerate(names_file.readlines())
            )
        )


def get_parts_files(commercial_references):
    parts_files = []
    for commercial_reference in commercial_references:
        images_folder = f"{DEFAULT_PARTS_FOLDER}/{commercial_reference['commercial-reference']}/images"
        annotations_folder = f"{DEFAULT_PARTS_FOLDER}/{commercial_reference['commercial-reference']}/annotations"
        parts_files += [
            {
                "image": images_folder + "/" + image_file,
                "annotation": annotations_folder + "/" + annotation_file,
                "info": commercial_reference
            }
            for image_file, annotation_file in zip(os.listdir(images_folder), os.listdir(annotations_folder))
        ]
    return parts_files


def get_label_data_from_xml(annotation_path, label_idx):
    with open(annotation_path) as annotation:
        annotation = xmlparse(annotation.read())
        annotation = annotation["annotation"]
        img_width = float(annotation["size"]["width"])
        img_height = float(annotation["size"]["height"])
        x_min = float(annotation["object"]["bndbox"]["xmin"])
        x_max = float(annotation["object"]["bndbox"]["xmax"])
        y_min = float(annotation["object"]["bndbox"]["ymin"])
        y_max = float(annotation["object"]["bndbox"]["ymax"])
        width = (x_max - x_min) / img_width
        height = (y_max - y_min) / img_height
        x_center = (x_max + x_min) / (2 * img_width)
        y_center = (y_max + y_min) / (2 * img_height)
        neg_flag = False
        for i in [x_center, y_center, width, height]:
            if i < 0 or i > 1:
                neg_flag = True
                break
        return (list(map(str, [label_idx, x_center, y_center, width, height])), neg_flag)


def create_configuration_file(train_dir, timestamp, num_classes):
    filters = num_classes * (num_classes + 5)
    with open(DEFAULT_CONFIG_TEMPLATE) as config_template_file:
        config_template = config_template_file.read()
        while '$NUM_CLASSES' in config_template:
            config_template = config_template.replace("$NUM_CLASSES", str(num_classes))
        while '$FILTERS' in config_template:
            config_template = config_template.replace("$FILTERS", str(filters))
        print(f"Creating yolov3 config file: ./config/yolov3-train-{timestamp}.cfg")
        new_config_file = open(f"{train_dir}/yolov3-train-{timestamp}.cfg", "w+")
        new_config_file.write(config_template)


def create_train_valid_split_files(train_dir, images, split):
    label_image_map = {label: [] for image, label in images}
    train = []
    valid = []
    for image, label in images:
        label_image_map[label] += [image]
    for label in label_image_map.keys():
        num_files = len(label_image_map[label])
        train_split = int((split / 100) * num_files)
        print(f"Image split for label {label}:\nTotal: {str(num_files)}\nTrain: {str(train_split)}\nValid: {str(num_files - train_split)}")
        train += label_image_map[label][:train_split]
        valid += label_image_map[label][train_split+1:]
    print(f"Creating {train_dir}/train.txt")
    with open(f"{train_dir}/train.txt", "w+") as train_file:
        train_file.write("\n".join(train))
    print(f"Creating {train_dir}/valid.txt")
    with open(f"{train_dir}/valid.txt", "w+") as valid_file:
        valid_file.write("\n".join(valid))


def create_dataset(timestamp, part_files, split, part_names_file_path, num_classes):
    train_dir = f"{DEFAULT_DATASET_PATH}/train-{timestamp}"
    images_dir = f"{train_dir}/images"
    labels_dir = f"{train_dir}/labels"
    create_directory(images_dir)
    create_directory(labels_dir)
    shutil.copy(part_names_file_path, f"{train_dir}/train-{timestamp}.names")
    num_part_files = len(part_files)
    f_image_path = images_dir + "/{:0" + str(len(str(num_part_files))) + "}.png"
    f_label_path = labels_dir + "/{:0" + str(len(str(num_part_files))) + "}.txt"
    images = []
    for idx, part_file in enumerate(part_files):
        image_path = f_image_path.format(idx)
        label_path = f_label_path.format(idx)
        label_data, neg_flag = get_label_data_from_xml(part_file["annotation"], part_file["info"]["label"])
        if neg_flag:
            print(f"Skipping {part_file}")
            continue
        print(f"Creating Label File:\n{label_path}")
        label_file = open(label_path, "w+")
        label_file.write(" ".join(label_data))
        label_file.close()
        print(f"Moving Image:\n{part_file['image']}\n{image_path}")
        shutil.copy(part_file["image"], image_path)
        images += [(image_path.replace(DEFAULT_DATASET_PATH, ".."), label_data[0])]
    create_train_valid_split_files(train_dir, images, split)
    create_configuration_file(train_dir, timestamp, num_classes)
    print(f"Creating data file:\n{train_dir}/train-{timestamp}.data")
    with open(f"{train_dir}/train-{timestamp}.data", "w+") as info_file:
        display_dir = train_dir.replace(DEFAULT_DATASET_PATH, "..")
        info_file.write(f"classes={str(num_classes)}\ntrain={display_dir}/train.txt\nvalid={display_dir}/valid.txt\nnames={display_dir}/train-{timestamp}.names")
    shutil.make_archive(f"train-{timestamp}", 'zip', train_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--names-path", type=str, default="./data/parts.names", help="Path to the name classes")
    parser.add_argument("--split", type=int, default=80, help="Percentage split for training (0-100)")
    opt = parser.parse_args()
    print("Generating new dataset")
    commercial_references = get_commercial_references_from_names_file(opt.names_path)
    part_files = get_parts_files(commercial_references)
    timestamp = str(datetime.utcnow().date())
    duplicates = len([directory for directory in os.listdir(DEFAULT_DATASET_PATH) if timestamp in directory])
    timestamp += f"-{str(duplicates)}"
    print(opt.split)
    create_dataset(timestamp, part_files, opt.split, opt.names_path, len(commercial_references))
    print("Dataset ready for training")

