import argparse
from string import Template
from os.path import abspath

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", type=str, default="config/ssd_mobilenet_v2_coco.config.template", help="Path to config template")
    parser.add_argument("--output", type=str, default="train-tf/training/pipeline.config")
    parser.add_argument("--num-class", type=str, default="3", help="Number of classes")
    parser.add_argument("--checkpoint-path", type=str, default="models/ssd_mobilenet_v2_coco_2018_03_29/model.ckpt", help="Path of starting checkpoint")
    parser.add_argument("--train-record-path", type=str, default="train-tf/data/train.record", help="Path of train record file")
    parser.add_argument("--test-record-path", type=str, default="train-tf/data/eval.record", help="Path of test record file")
    parser.add_argument("--pb-txt-path", type=str, default="train-tf/data/labels.pbtxt", help="Path of pbtxt file")
    opt = parser.parse_args()
    with open(opt.path) as config_template_file:
        config_template = Template(config_template_file.read())
        config_template_file.close()
        content = config_template.safe_substitute(
            num_class=opt.num_class,
            checkpoint_path=abspath(opt.checkpoint_path),
            train_record_path=abspath(opt.train_record_path),
            test_record_path=abspath(opt.test_record_path),
            pb_txt_path=abspath(opt.pb_txt_path)
        )
        output_file = open(opt.output, "w+")
        output_file.write(content)
        output_file.close()
        print(f"Created {opt.output}")
